<?php
 //Define location of required files in baltauto directory
require (dirname(__FILE__) . '/../includes/config.php');


//Change the PHP script timeout to infinite
set_time_limit(0);

//Increase the memory limit
ini_set('memory_limit', '-1');

$running_log = '';
$running_msg = array();
$newproducts = 0;
$updatedproducts = 0;
$lines = 0;
$query = array();
$progress = 0;
$error = 0;

$temp_table_name = $SUPPLIER_SHORT_NAME."_temp";


$time_start = microtime(true);



$running_log .= "\nStarting...";
array_push($running_msg, "Starting...");
$local_file = dirname(__FILE__) ."/../downloads/".$SUPPLIER_SHORT_NAME."feed-FULL-DETAILS-".date('m-d-Y-h.i.s').".csv";
$local_file2 = dirname(__FILE__) ."/../downloads/".$SUPPLIER_SHORT_NAME."feed-FULL-".date('m-d-Y-h.i.s').".csv";
$DownloadFile = dirname(__FILE__) ."/../downloads/".$SUPPLIER_SHORT_NAME."feed-FULL-".date('m-d-Y-h.i.s').".xlsx";



//****This function will kill the script if the feed file hasn't been updated since the last sync************
//Copy file if this is the first time file was submitted
if (copy( $ftp_filename,$DownloadFile)) {
    $running_log .= "\nSuccessfully downloaded update file to $local_file";
    array_push($running_msg, "Successfully downloaded update file to $local_file");
} else {
    $running_log .= "\nFailed to copy the file";
    array_push($running_msg, "Failed to copy the file");
    $message = "The script failed to download the update file.  The log below should show any errors that were captured:<br><br>".$running_log;
    mail($mailTo, "$SUPPLIER_NAME Sync Script Failed", $message);    
    die($message);
}

//Hash file to determine if file changed
fileUpdated_2($DownloadFile,1);

//Convert file into .CSV
convertXLStoCSV_Multi_Sheet($DownloadFile, $local_file,1);
convertXLStoCSV_Multi_Sheet($DownloadFile, $local_file2,0);
unlink($DownloadFile);

//die($running_log;
//convert csv file content to an object
$feedObject = createFileObject($local_file,",",2);
$feedObject2 = createFileObject($local_file2,",",2);

//create array of FULL file to use in the update as a lookup. The key should be the item #
$running_log .= "\nCreate array of FULL file to use in the update as a lookup...";

$item_lookup_array= array();
foreach ($feedObject2 as $item2) {
	//print_r($item2);
	//die();
	    if ($item2[1] != '' ) { 

		$item_lookup_array[$item2[1]] = array(
		'Description'=> $item2[2],
		'Price'=> $item2[3],
		'UPC Code'=> $item2[4],
		'Mult'=> $item2[5],
		'Min'=> $item2[6],
		'Page#'=> $item2[7],
		'Case Qty'=> $item2[8],
		'Ounces'=> $item2[9],
		
			);
		}
}
//print_r($item_lookup_array);
//die();
/*FEED INTEGRATY CHECK*/
    if (check_feed_integraty($feedObject,$feed_columns)) {
        $running_log .= "\nFeed Column Match to Expected";
    } else {
	        $running_log .= "\nFeed Column count does not match feed expected column count";
			$message = "The $SUPPLIER_NAME FEED DISCREPENCY ERROR.  The script log is below:\n".$running_log;
			$subject ="$SUPPLIER_NAME FEED DISCREPENCY ERROR!";		
			Henry_Email($subject, $message,$mailto,$cc,$bcc,false,true);
			echo  $running_log ;

			syncFatalError(1, $running_log);
		
    }
    if (check_feed_integraty_size($local_file,$feed1_size_sg)) {
        $running_log .= "\nFeed Passed Size Safeguard Check";
    } else {
	        $running_log .= "\nFeed Size Failed Safeguard Check -- filesize($local_file)";
			$message = "The $SUPPLIER_NAME FEED SIZE SAFEGUARD ERROR.  The script log is below:\n".$running_log;
			$subject ="$SUPPLIER_NAME FEED SIZE SAFEGUARD ERROR!";		
			Henry_Email($subject, $message,$mailto,$cc,$bcc,false,true);
			echo  $running_log ;
			syncFatalError(1, $running_log);
		
    }
/*----------------------------*/
while (!$feedObject->eof()) { 
    $lines++; 
    $feedObject->fgetcsv(); 
}

$lines--;

$running_log .= "\nThe file object contains " . $lines . " items.";
 array_push($running_msg, "The file object contains " . $lines . " items.");
/**/
if ($lines > 1) {
    $running_log .= "\nThe file object was successfully created.  Uploading feed file to S3 for archiving";
    array_push($running_msg, "The file object was successfully created.  Uploading feed file to S3 for archiving");
    if (SendToS3($local_file, $bucket_name, $access_policy)) {
        $running_log .= "\nFile successfully copied to S3 {$bucket_name}/".baseName($local_file).PHP_EOL;
         array_push($running_msg, "File successfully copied to S3 {$bucket_name}/".baseName($local_file));
    } else {
        $running_log .= "\nFailed to copy the file to S3!";
        array_push($running_msg, "Failed to copy the file to S3!");
 }
} else {
    $running_log .= "\nThere was a problem creating the file object!";
    array_push($running_msg, "There was a problem creating the file object!");
       syncFatalError(1, $running_log);

}


//Drop the temp table
$dropTempQuery = "DROP TABLE IF EXISTS " . $temp_table_name;
if (!mysqli_query($link, $dropTempQuery)) { //Drop table failed.  Stop the update.
    $running_log .= "Failed to drop the TEMP table.";
    array_push($running_msg,"Failed to drop the TEMP table.");
            
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
       syncFatalError(1, $running_log);

} 


//Create the temp table
$createTempTableQuery = "CREATE TABLE IF NOT EXISTS " . $temp_table_name . " (
                    `ProdID` int(11) NOT NULL auto_increment,
                    `VenderSKU` varchar(50) NOT NULL default '',					
                    `Manufacturer` varchar(40) default NULL,
                    `ProdName` varchar(200) NOT NULL default '',
                    `ProdLongDesc` text,
                    `Material` text,
                    `ProdSKU` varchar(50) default NULL,
                    `UPC` varchar(20) default NULL,
                    `Mult` int(11) NOT NULL default '0',
                    `Min` int(11) NOT NULL default '0',
                    `FK_ProdCatID` int(11) NOT NULL default '0',
                    `FK_ConditionID` int(11) NOT NULL default '0',
                    `MAAP` double default '0',
                    `MSRP` double default '0',
                    `ProdWeight` double(15,2) default '0.00',
                    `ProdLength` double(15,2) default '0.00',
                    `ProdWidth` double(15,2) default '0.00',
                    `ProdHeight` double(15,2) default '0.00',
                    `ProdQuantity` int(11) NOT NULL default '0',
                    `CaseQty` int(11) NOT NULL default '0',
                    `WholeSalePrice` double(15,2) default NULL,
                                   `Key_Features` text,
                                   `Key_Features2` text,
                                   `Key_Features3` text,
                                   `Key_Features4` text,
                                   `Key_Features5` text,
                                   `image1` varchar(200) NOT NULL default '',                  
                                   `image2` varchar(200) NOT NULL default '',                                                             
			           `is_updated` int(1) default 0,
                                   PRIMARY KEY  (`ProdID`)
                  );";

//determine if Temp Table was successfully created
if (!mysqli_query($link, $createTempTableQuery)) {
    $running_log .= "\nFailed to create the TEMP table.";
    array_push($running_msg,"Failed to create the TEMP table.");
    
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg,"The SQL error was " . mysqli_error($link) . ".");
    
    syncFatalError(1, $running_log);

} else {
    $running_log .= "\nTEMP table was successfully created.";
     array_push($running_msg,"TEMP table was successfully created.");
}

/*

		$item_lookup_array[$item2[1]] = array(
		'Description'=> $item2[2],
		'Price'=> $item2[3],
		'UPC Code'=> $item2[4],
		'Mult'=> $item2[5],
		'Min'=> $item2[6],
		'Page#'=> $item2[7],
		'Case Qty'=> $item2[8],
		'Ounces'=> $item2[9],
*/

foreach ($feedObject as $item) {
    
    $progress++;


	//DEBUG
	//print_r($item);
    //die();
    //Check if the row is blank
    if ($item[0] != '' ) { 
	    
        
       // $VenderSKU = mysqli_real_escape_string($link, $item[0]).mysqli_real_escape_string($link, $item[16]);
        $VenderSKU = mysqli_real_escape_string($link, $item[0]);
        
        //Set Baltauto ProdSKU to Manufacturer Part Number
        $ProdSKU = mysqli_real_escape_string($link, $item[0]);		
		if ($ProdSKU == '') {
			$ProdSKU = $VenderSKU;
		}		
       		
	   $ProductIDType = "";
      
  	  $WholeSalePrice = mysqli_real_escape_string($link, $item_lookup_array[$VenderSKU]['Price']); 
        
        //$ProdQuantity = mysqli_real_escape_string($link, $item[0]);
        $ProdQuantity = 0;
        $Mult = mysqli_real_escape_string($link, $item[4]);
        $Min = mysqli_real_escape_string($link, $item[5]);
        $CaseQty = mysqli_real_escape_string($link, $item[7]);
        $Material = mysqli_real_escape_string($link, $item[24]);

        $MSRP = "";
        $MAP = 0;
        if($MAP === ""){
            $MAP = 0;
           }
        
		$UPC = mysqli_real_escape_string($link, $item[3]);
        $UPC = ltrim($UPC, '0');
		///need to fix upx to get to right length			
			if (strlen($UPC) == 10) $UPC="00".$UPC;
			elseif (strlen($UPC) == 11) $UPC="0".$UPC;
			elseif (strlen($UPC) == 9) $UPC="000".$UPC;
 
        $KeyFeatures = "";
        $KeyFeatures2 = "";
        $KeyFeatures3 = "";
        $KeyFeatures4 = "";
        $KeyFeatures5 = "";
       
	   
        $Manufacturer = "SPOONTIQUES";
        $Manufacturer = strtoupper($Manufacturer);
        $image1  = "";
        $image2  = "";

        
        $ProdLength = mysqli_real_escape_string($link, $item[15]);
        $ProdWidth = mysqli_real_escape_string($link, $item[16]);
        $ProdHeight = mysqli_real_escape_string($link, $item[17]);
        $ProdWeight = mysqli_real_escape_string($link, $item[18]);
        
        //Determine if MAP is blank if yes set to zero


       
		
        //$Category1 = mysqli_real_escape_string($link, $item[1]); 
        // $Category1 = "";
 
		

		$TEMP = TRIM(mysqli_real_escape_string($link, $item[1]));
		

		if ($TEMP ==""){ 
                      $TEMP  = $Manufacturer." ".$ProdSKU;
		       //echo "$VenderSKU -- $ProdSKU\n";
                        }
	        if (strpos($TEMP, $ProdSKU) !== true) {
				$TEMP = $ProdSKU." ".$TEMP;
			}
		if($Manufacturer<> "") {
			if (strpos($TEMP, $Manufacturer) !== true) {
				$TEMP = $Manufacturer." ".$TEMP;
			}		
		}
		$ProdName = strtoupper(TRIM($TEMP));
		$ProdName = preg_replace('!\s+!', ' ', $ProdName);	

       
       
          $ProdLongDesc = mysqli_real_escape_string($link, $item[1]);
		  
   //If Material value present append to Product Description                		  
               if($Material !== ""){
                        $ProdLongDesc .= '<li>'."Material: ".$Material.'</li>';
                    }   
                           
		$FK_ConditionID = 1;

		
        $query[] = "INSERT INTO " . $temp_table_name . " 
		(Manufacturer, 
		ProdName,
		WholeSalePrice, 
		ProdSKU, 
		UPC, 
		FK_ProdCatID, 
		FK_ConditionID, 
		MAAP,
		MSRP,
        ProdWeight,
		ProdLength,
		ProdWidth,
		ProdHeight,
		ProdQuantity, 
		ProdLongDesc, 
		Mult, 
		VenderSKU, 
		CaseQty, 
		Material, 
		Min, 
                Key_Features,
                Key_Features2,
                Key_Features3,
                Key_Features4,
                Key_Features5,
                image1,
                image2,
                is_updated
		) 
		VALUES (
		'$Manufacturer', 
		'$ProdName', 
		'$WholeSalePrice', 
		'$ProdSKU', 
		'$UPC', 
		0, 
		1,
		'$MAP',
        '$MSRP',
		'$ProdWeight',
		'$ProdLength',
		'$ProdWidth',
		'$ProdHeight',
		'$ProdQuantity', 
		'$ProdLongDesc', 
		'$Mult', 
		'$VenderSKU', 
		'$CaseQty', 
		'$Material', 
		'$Min', 
                '$KeyFeatures',
                '$KeyFeatures2',                     
                '$KeyFeatures3',
                '$KeyFeatures4',
                '$KeyFeatures5',                       
                '$image1',
                '$image2',
                0   
		)";
	}
    
    if (count($query) == 1000) {
        
        //DEBUG
        //echo $query;
        
        $query_string = implode(';', $query);
        
        $i = 0;
        
        //After we have 1000 products, do a bulk insert
        if (mysqli_multi_query($link, $query_string)) {
            do {
                mysqli_next_result($link);
                $i++;
            }
            
            while( mysqli_more_results($link) );
        }
        
        if (mysqli_errno($link)) {
            $running_log .= "\nEncountered a SQL error!";
            array_push($running_msg,"Encountered a SQL error!");
            $running_log .= "\nQuery number " . $i . " of 1000 failed";
            array_push($running_msg, "Query number " . $i . " of 1000 failed");
            $running_log .= "\nFocus around line " . ($progress - (1000 - $i)) . " of " . $lines . " in the feed";
            array_push($running_msg, "Focus around line " . ($progress - (1000 - $i)) . " of " . $lines . " in the feed");
            $running_log .= "\nContext:";
            array_push($running_msg, "Context:");
            $running_log .= "\nQuery # " . ($i - 7) . " = " . $query[($i - 7)];
            array_push($running_msg, "Query # " . ($i - 7) . " = " . $query[($i - 7)]);
            $running_log .= "\nQuery # " . ($i - 6) . " = " . $query[($i - 6)];
            array_push($running_msg, "Query # " . ($i - 6) . " = " . $query[($i - 6)]);
            $running_log .= "\nQuery # " . ($i - 5) . " = " . $query[($i - 5)];
            array_push($running_msg, "Query # " . ($i - 5) . " = " . $query[($i - 5)]);
            $running_log .= "\nQuery # " . ($i - 4) . " = " . $query[($i - 4)];
            array_push($running_msg, "Query # " . ($i - 4) . " = " . $query[($i - 4)]);
            $running_log .= "\nQuery # " . ($i - 3) . " = " . $query[($i - 3)];
            array_push($running_msg, "Query # " . ($i - 3) . " = " . $query[($i - 3)]);
            $running_log .= "\nQuery # " . ($i - 2) . " = " . $query[($i - 2)];
            array_push($running_msg, "Query # " . ($i - 2) . " = " . $query[($i - 2)]);
            $running_log .= "\nQuery # " . ($i - 1) . " = " . $query[($i - 1)];
            array_push($running_msg, "Query # " . ($i - 1) . " = " . $query[($i - 1)]);
            $running_log .= "\nQuery # " . $i . " = " . $query[$i];
            array_push($running_msg, "Query # " . $i . " = " . $query[$i]);
            $running_log .= "\nQuery # " . ($i + 1) . " = " . $query[($i + 1)];
            array_push($running_msg, "Query # " . ($i + 1) . " = " . $query[($i + 1)]);
            $running_log .= "\nQuery # " . ($i + 2) . " = " . $query[($i + 2)];
            array_push($running_msg, "Query # " . ($i + 2) . " = " . $query[($i + 2)]);
            $running_log .= "\nError = " . mysqli_error($link);
            array_push($running_msg, "Error = " . mysqli_error($link));
            echo $running_log;
            $message = "The script failed during a SQL query.  The log below should show any errors that were captured:<br><br>".$running_log;
            mail($mailTo, "Green Supply Script Failed", $message);    
            die();        
        }           

        unset($query);
    }
}

//Take care of any leftovers
$query_string = implode(';', $query);
$i = 0;
        
if (mysqli_multi_query($link, $query_string)) {
    do {
        mysqli_next_result($link);
        $i++;
    }
            
        while( mysqli_more_results($link) );
}
        
if (mysqli_errno($link)) {
    $running_log .= "\nEncountered a SQL error!";
     array_push($running_msg,"Encountered a SQL error!");
    $running_log .= "\nQuery number " . $i . " of 1000 failed";
     array_push($running_msg, "Query number " . $i . " of 1000 failed");
    $running_log .= "\nFocus around line " . ($progress - (1000 - $i)) . " of " . $lines . " in the feed";
     array_push($running_msg, "Focus around line " . ($progress - (1000 - $i)) . " of " . $lines . " in the feed");
    $running_log .= "\nContext:";
     array_push($running_msg, "Context:");
    $running_log .= "\nQuery # " . ($i - 7) . " = " . $query[($i - 7)];
    array_push($running_msg, "Query # " . ($i - 7) . " = " . $query[($i - 7)]);
    $running_log .= "\nQuery # " . ($i - 6) . " = " . $query[($i - 6)];
    array_push($running_msg, "\nQuery # " . ($i - 6) . " = " . $query[($i - 6)]);
    $running_log .= "\nQuery # " . ($i - 5) . " = " . $query[($i - 5)];
    array_push($running_msg, "\nQuery # " . ($i - 5) . " = " . $query[($i - 5)]);
    $running_log .= "\nQuery # " . ($i - 4) . " = " . $query[($i - 4)];
    array_push($running_msg, "\nQuery # " . ($i - 4) . " = " . $query[($i - 4)]);
    $running_log .= "\nQuery # " . ($i - 3) . " = " . $query[($i - 3)];
    array_push($running_msg, "\nQuery # " . ($i - 3) . " = " . $query[($i - 3)]);
    $running_log .= "\nQuery # " . ($i - 2) . " = " . $query[($i - 2)];
    array_push($running_msg, "\nQuery # " . ($i - 2) . " = " . $query[($i - 2)]);
    $running_log .= "\nQuery # " . ($i - 1) . " = " . $query[($i - 1)];
    array_push($running_msg, "\nQuery # " . ($i - 1) . " = " . $query[($i - 1)]);
    $running_log .= "\nQuery # " . $i . " = " . $query[$i];
    array_push($running_msg, "\nQuery # " . $i . " = " . $query[$i]);
    $running_log .= "\nQuery # " . ($i + 1) . " = " . $query[($i + 1)];
    array_push($running_msg, "\nQuery # " . ($i + 1) . " = " . $query[($i + 1)]);
    $running_log .= "\nQuery # " . ($i + 2) . " = " . $query[($i + 2)];
    array_push($running_msg, "\nQuery # " . ($i + 2) . " = " . $query[($i + 2)]);
    $running_log .= "\nError = " . mysqli_error($link);
    array_push($running_msg, "\nError = " . mysqli_error($link));
    echo $running_log;
    $message = "The script failed during a SQL query.  The log below should show any errors that were captured:<br><br>".$running_log;
    mail($mailTo, "Green Supply Script Failed", $message);    
    die();        
}           

$running_log .= "\nFeed loaded into the TEMP table.";
array_push($running_msg,"Feed loaded into the TEMP table."); 
//DELETE DUPLICATES
$startQuery = " delete  " . $temp_table_name . " 
   from  " . $temp_table_name . " 
  inner join (
     select max(ProdID) as lastId, VenderSKU
       from  " . $temp_table_name . " 
      group by `VenderSKU`
     having count(*) > 1) duplic on duplic.VenderSKU =  " . $temp_table_name . " .VenderSKU
  where  " . $temp_table_name . " .ProdID < duplic.lastId;";

if (!mysqli_query($link, $startQuery)) {
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to deleted duplicate skus in locked in Skus.";
    array_push($running_msg,"Failed to deleted duplicate skus in locked in Skus.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg,"The SQL error was " . mysqli_error($link) . ".");
    $error = 1;
    
    if (mysqli_errno($link) == "1205") {
        $running_log .= "\nThe products table is currently locked.  Will try again later";
        array_push($running_msg,"The products table is currently locked.  Will try again later");
        syncFatalError(1, $running_log);        
    }
    
    syncFatalError(1, $running_log);  
} else {
    $flag_count = mysqli_affected_rows($link);
    $running_log .= "\nSuccessfully deleted duplicate skus " . $flag_count . " products.";
    array_push($running_msg,"Successfully deleted duplicate skus " . $flag_count . " products.");
}

//TESTING ; when testing this is good to uncomment to kill the script right here after all data is imported into the temp table
//die("TESTING: ".$running_log);

//Set the flag for updates
$startQuery = "UPDATE " . $products_table_name . " SET LOCK_UPDATE_QUAN = 1 WHERE WholeSaler = " . $wholesalerID;

if (!mysqli_query($link, $startQuery)) {
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to set the update flag.";
    array_push($running_msg, "\nFailed to set the update flag.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    $error = 1;
    
    if (mysqli_errno($link) == "1205") {
        $running_log .= "\nThe products table is currently locked.  Will try again later";
        array_push($running_msg, "The products table is currently locked.  Will try again later");
           syncFatalError(1, $running_log);
        
    }
    
       syncFatalError(1, $running_log);
  
} else {
    $flag_count = mysqli_affected_rows($link);
    $running_log .= "\nSuccessfully set the update flag on for " . $flag_count . " products.";
    array_push($running_msg, "Successfully set the update flag on for " . $flag_count . " products.");
}



//Normalize Products in the temp table
/**/
$normalize_query = "UPDATE " . $temp_table_name . " p, Duplicate_ManNormalize dm SET p.Manufacturer =  dm.Manu_Fix WHERE dm.Manu_Check = p.Manufacturer";

if (!mysqli_query($link, $normalize_query)) {
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to normalize the temporary products table.";
    array_push($running_msg, "Failed to normalize the temporary products table.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    $error = 1;
} else {
    $normalize_count = mysqli_affected_rows($link);
    $running_log .= "\nSuccessfully normalized " . $normalize_count . " products in the temporary products table.";
    array_push($running_msg,"Successfully normalized " . $normalize_count . " products in the temporary products table.");
}


//Delete products from bad Manufacturers in the temp table
$deleteManufacturersQuery = "DELETE FROM " . $temp_table_name . " WHERE Manufacturer IN (SELECT ManuName FROM Manufacturer_Delete WHERE WholeSaler = " . $wholesalerID . " OR WholeSaler = 0);";

if (!mysqli_query($link, $deleteManufacturersQuery)) {
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to delete products from bad manufacturers in the temporary products table.";
    array_push($running_msg, "Failed to delete products from bad manufacturers in the temporary products table.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";   
    array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    $error = 1;
} else {
    $bad_count = mysqli_affected_rows($link);
    $running_log .= "\nSuccessfully deleted " . $bad_count . " products from bad manufacturers in the temporary products table.";
    array_push($running_msg,"Successfully deleted " . $bad_count . " products from bad manufacturers in the temporary products table.");
}
    
$running_log .= "\n\nUpdating product details, quantities, weights, length, hieght and locations."; 
array_push($running_msg, "Updating product details, quantities, weights, length, hieght and locations."); 
    
//Update the product details from temp table
$updateDetailQuery = "UPDATE " . $products_table_name . " p, " . $temp_table_name . " t SET p.ProdName = t.ProdName, p.FK_ConditionID = t.FK_ConditionID, p.ProdLongDesc = t.ProdLongDesc, p.UPC = t.UPC, p.FK_ProdCatID = t.FK_ProdCatID, p.ProdSKU = t.ProdSKU, p.Manufacturer = t.Manufacturer, p.TimeStamp = NOW() WHERE p.VenderSKU = t.VenderSKU  AND p.WholeSaler = ".$wholesalerID;
if (!mysqli_query($link, $updateDetailQuery)) {
    $running_log .= "\nFailed to update the product details and quantities.";
    array_push($running_msg,"Failed to update the product details and quantities.");
    
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg,"The SQL error was " . mysqli_error($link) . ".");
        
    if (mysqli_errno($link) == "1205") {
        $running_log .= "\nThe products table is currently locked.  Will try again later";
        array_push($running_msg,"The products table is currently locked.  Will try again later");
        
           syncFatalError(1, $running_log);
        
    }
        
       syncFatalError(1, $running_log);

} else {
    $running_log .= "\nSuccessfully updated the product details and quantities.";
    array_push($running_msg, "Successfully updated the product details and quantities.");
    $update_count = mysqli_affected_rows($link);        
}
    
    
//Update the Weights from the temp table
$updateWeightQuery = "UPDATE " . $products_table_name . " p, " . $temp_table_name . " t SET p.ProdWeight = t.ProdWeight WHERE p.VenderSKU = t.VenderSKU  AND p.WholeSaler = " . $wholesalerID . " AND p.LOCK_WEIGHT = 0 AND t.ProdWeight != 0";
if (!mysqli_query($link, $updateWeightQuery)) {
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to update the product weights.";
    array_push($running_msg, "Failed to update the product weights.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
     array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    $error = 1;
    if (mysqli_errno($link) == "1205") {
        $running_log .= "\nThe products table is currently locked.  Will try again later";
        array_push($running_msg, "The products table is currently locked.  Will try again later");
           syncFatalError(1, $running_log);
        
    }
} else {
    $running_log .= "\nSuccessfully updated the product weights."; 
    array_push($running_msg, "Successfully updated the product weights.");
}

//Update the length and height from the temp table
$updateWeightQuery = "UPDATE " . $products_table_name . " p, " . $temp_table_name . " t SET p.ProdLength = t.ProdLength, p.ProdHeight = t.ProdHeight, p.ProdWidth = t.ProdWidth WHERE p.VenderSKU = t.VenderSKU  AND p.WholeSaler = " . $wholesalerID . " AND p.LOCK_WEIGHT = 0 AND t.ProdWeight != 0";
if (!mysqli_query($link, $updateWeightQuery)) {
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to update the product width, length and height.";
    array_push($running_msg, "Failed to update the product width, length and height.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
     array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    $error = 1;
    if (mysqli_errno($link) == "1205") {
        $running_log .= "\nThe products table is currently locked.  Will try again later";
        array_push($running_msg, "The products table is currently locked.  Will try again later");
           syncFatalError(1, $running_log);
        
    }
} else {
    $running_log .= "\nSuccessfully updated the product width, length and height."; 
    array_push($running_msg, "Successfully updated the product width, length and height.");
}
   
    
//Update Item locations table from the temp table
/*
$countLocationQuery = "SELECT * FROM " . $location_table_name . " WHERE FK_WholesalerID = " . $wholesalerID;
if (!mysqli_query($link, $countLocationQuery)){
    $running_log .= "\nFailed to select the number of products in the locations table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        
       syncFatalError(1, $running_log);

} else {
    $location_count = mysqli_affected_rows($link);
}
    
$deleteLocationsQuery = "DELETE FROM " . $location_table_name . " WHERE FK_WholesalerID = " . $wholesalerID;
if (!mysqli_query($link, $deleteLocationsQuery)) {
    $running_log .= "\nFailed to delete from the product locations table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        
       syncFatalError(1, $running_log);

} else {
    $delete_count = mysqli_affected_rows($link);
        
    if ($location_count != $delete_count) {
        $running_log .= "\nThe number of rows deleted from the locations table does not equal what should have been deleted.  Something happened.";
        $running_log .= "\nThere were " . $location_count . " items in the location table but only " . $delete_count . " items were deleted.";
           syncFatalError(1, $running_log);

    } else {
        $running_log .= "\nSuccessfully deleted the old product locations.";
    }
        
}

$updateLocationsQuery = "INSERT INTO " . $location_table_name . " (VenderSKU,FK_WholesalerID,LOC1) (SELECT VenderSKU, " . $wholesalerID . ", LOC1 FROM " . $temp_table_name . ")";
if (!mysqli_query($link, $updateLocationsQuery)){
    $running_log .= "\nFailed to update the locations table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
      
       syncFatalError(1, $running_log);

} else {
    $running_log .= "\nSuccessfully updated the current product locations.";
}

*/
//Update the prices from the temp table
$updatePriceQuery = "UPDATE " . $products_table_name . " p, " . $temp_table_name . " t SET /*p.WholeSalePrice = t.WholeSalePrice,*/ p.LOCK_DISC = 0, p.LOCK_UPDATE_QUAN = 0, p.Active = 1 WHERE p.VenderSKU = t.VenderSKU  AND p.WholeSaler = " . $wholesalerID . " AND p.LOCK_UPDATE_PRICE = 0";
if (!mysqli_query($link, $updatePriceQuery)){
    $running_log .= "\nFailed to update the product prices.";
    push($running_msg, "Failed to update the product prices.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    if (mysqli_errno($link) == "1205") {
        $running_log .= "\nThe products table is currently locked.  Will try again later";
        push($running_msg,"The products table is currently locked.  Will try again later");
           syncFatalError(1, $running_log);
        
    }
       syncFatalError(1, $running_log);

} else {
    $running_log .= "\nSuccessfully updated the product prices.";
    array_push($running_msg,"Successfully updated the product prices.");
    $update_count = mysqli_affected_rows($link);
}

//Update MAP pricing from the temp table

$truncateMAPQuery = "TRUNCATE TABLE " . $map_table_name;
if (!mysqli_query($link, $truncateMAPQuery)) {
    $running_log .= "\nFailed to truncate the MAP table.";
    array_push($running_msg,"Failed to truncate the MAP table.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg,"The SQL error was "); 
    
    syncFatalError(1, $running_log);
    
} else {
    $running_log .= "\nSuccessfully truncated the MAP table.";
    array_push($running_msg,"Successfully truncated the MAP table."); 
}



$updateMAPQuery = "INSERT INTO " . $map_table_name . " (VenderSKU, MAP) (SELECT VenderSKU, MAAP FROM " . $temp_table_name . " WHERE MAAP > 0)";
if (!mysqli_query($link, $updateMAPQuery)) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the MAP prices.";
        array_push($running_msg,"Failed to update the MAP prices."); 
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        array_push($running_msg,"The SQL error was ");
    } else {
        $map_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the MAP prices for " . $map_count . " products."; 
         array_push($running_msg,"Successfully updated the MAP prices for " . $map_count . " products.");
}


//Update MSRP pricing from the temp table
/**/
$truncateMSRPQuery = "TRUNCATE TABLE " . $msrp_table_name;
if (!mysqli_query($link, $truncateMSRPQuery)) {
    $running_log .= "\nFailed to truncate the MSRP table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
     
       syncFatalError(1, $running_log);

    
} else {
    $running_log .= "\nSuccessfully truncated the MSRP table.";
}

$updateMSRPQuery = "INSERT INTO " . $msrp_table_name . " (VenderSKU, MSRP) (SELECT VenderSKU, MSRP FROM " . $temp_table_name . " WHERE MSRP > 0)";
if (!mysqli_query($link, $updateMSRPQuery)) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the MSRP prices.";
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    } else {
        $map_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the MSRP prices for " . $map_count . " products.";       
}


//Update categories from the temp table
/*$truncateCategoriesQuery = "TRUNCATE TABLE " . $categories_table_name;
if (!mysqli_query($link, $truncateCategoriesQuery)) {
	//This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to truncate the categories table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
     
    
} else {
    $running_log .= "\nSuccessfully truncated the categories table.";
}

$updateCategoriesQuery = "INSERT INTO " . $categories_table_name . " (VenderSKU, WholeSaleCatName1) (SELECT VenderSKU, Category1 FROM " . $temp_table_name . ")";

if (!mysqli_query($link, $updateCategoriesQuery)) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the categories.";
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    } else {
        $categories_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the categories for " . $categories_count . " products.";       
}


//SET UN-RESTRICTED ITEMS BASED ON CAT
/*
$updateCategoriesQuery = "UPDATE " . $categories_table_name . " SET restricted = 0 WHERE WholeSaleCatName1 IN (".$UN_RESTRICTED_CATS.")";

if (!mysqli_query($link, $updateCategoriesQuery)) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to set unrestricted categories.";
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    } else {
        $categories_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully set unrestricted categoriesfor " . $categories_count . " products.";       
}
*/

//Update images from the temp table
$truncateImagesQuery = "TRUNCATE TABLE " . $images_table_name;
if (!mysqli_query($link, $truncateImagesQuery)) {
	//This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to truncate the images table.";
    array_push($running_msg, "\nFailed to truncate the images table.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg, "\nThe SQL error was " . mysqli_error($link) . ".");
} else {
    $running_log .= "\nSuccessfully truncated the images table.";
    array_push($running_msg, "Successfully truncated the images table.");
}

$updateImagesQuery = "INSERT INTO " . $images_table_name . " (FK_ProdID, FK_VenderSKU, image1,image2) (SELECT ProdID, VenderSKU, image1,image2 FROM " . $temp_table_name . ")";
if (!mysqli_query($link, $updateImagesQuery)) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the images.";
        array_push($running_msg, "Failed to update the images.");
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    } else {
        $images_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the images_table_name for " . $images_count . " products.";
        array_push($running_msg, "Successfully updated the images_table_name for " . $images_count . " products.");
}

//Update notes_table_name from the temp table
/*
$truncateImagesQuery = "TRUNCATE TABLE " . $notes_table_name;
if (!mysqli_query($link, $truncateImagesQuery)) {
	//This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to truncate the notes_table_name table.";
    array_push($running_msg, "Failed to truncate the notes_table_name table.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg,"The SQL error was " . mysqli_error($link) . ".");
} else {
    $running_log .= "\nSuccessfully truncated the notes_table_name table.";
    array_push($running_msg,"Successfully truncated the notes_table_name table.");
}

$updateImagesQuery = "INSERT INTO " . $notes_table_name . " (FK_VenderSKU, ASIN) (SELECT VenderSKU, ASIN FROM " . $temp_table_name . " WHERE ASIN <>'')";

if (!mysqli_query($link, $updateImagesQuery)) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the notes_table_name.";
        array_push($running_msg,"Failed to update the notes_table_name.");
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    } else {
        $images_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the notes_table_name for " . $images_count . " products.";  
        array_push($running_msg,"Successfully updated the notes_table_name for " . $images_count . " products.");
}
*/

//Update products_table_name from the temp table
$deleteExistingQuery = "DELETE t from " . $products_table_name . " as p, " . $temp_table_name . " as t where p.WholeSaler = " . $wholesalerID . " and t.VenderSKU = p.VenderSKU;";
if (!mysqli_query($link, $deleteExistingQuery)) {
        $running_log .= "\nFailed to delete the updated products from the temp table.";
        array_push($running_msg,"Failed to delete the updated products from the temp table.");
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        array_push($running_msg,"The SQL error was " . mysqli_error($link) . ".");
        
        if (mysqli_errno($link) == "1205") {
            $running_log .= "\nThe products table is currently locked.  Will try again later";
            array_push($running_msg,"The products table is currently locked.  Will try again later");
               syncFatalError(1, $running_log);
        
        }
        
           syncFatalError(1, $running_log);

} else {
        $delete_count = mysqli_affected_rows($link);
        
        if (1==0) {
            if ($update_count != $delete_count) {
            $running_log .= "\nThe number of rows deleted from the temp table does not equal what should have been deleted.  Something happened.";
            array_push($running_msg,"The number of rows deleted from the temp table does not equal what should have been deleted.  Something happened.");
            $running_log .= "\nThere were " . $update_count . " items updated but only " . $delete_count . " items were deleted.";
            array_push($running_msg,"There were " . $update_count . " items updated but only " . $delete_count . " items were deleted.");
            syncFatalError(1, $running_log);

        } else {
            $running_log .= "\nSuccessfully deleted " . $delete_count . " updated products from the temp table.";
            array_push($running_msg,"Successfully deleted " . $delete_count . " updated products from the temp table.");
        }
    }    
}


//Insert New Products
$insertNewQuery = "INSERT INTO " . $products_table_name . " (Manufacturer, ProdName, WholeSalePrice, ProdSKU, UPC, FK_ProdCatID, FK_ConditionID, ProdLongDesc, ProdWeight, ProdLength, ProdHeight,ProdQuantity, WholeSaler, VenderSKU, TimeStamp, LOCK_UPDATE_QUAN) (SELECT Manufacturer, ProdName, WholeSalePrice, ProdSKU, UPC, FK_ProdCatID, FK_ConditionID, ProdLongDesc, ProdWeight, ProdLength, ProdHeight, ProdQuantity, " . $wholesalerID . ", VenderSKU, 'NOW()', 0 FROM " . $temp_table_name . ");";

if (!mysqli_query($link, $insertNewQuery)) {
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to insert new products.";
    array_push($running_msg, "Failed to insert new products.");
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    $error = 1;
    if (mysqli_errno($link) == "1205") {
            $running_log .= "\nThe products table is currently locked.  Will try again later";
            array_push($running_msg, "The products table is currently locked.  Will try again later");
               syncFatalError(1, $running_log);
        
    }
} else {
    $newproducts = mysqli_affected_rows($link);
    $running_log .= "\nSuccessfully inserted " . $newproducts . " new products.";
    array_push($running_msg, "Successfully inserted " . $newproducts . " new products.");
}


//Update FK_ProdID in Locations Table
/*
$updateLocationProdIDQuery = "UPDATE " . $location_table_name . " l JOIN " . $products_table_name . " p on (l.VenderSKU = p.VenderSKU AND l.FK_WholesalerID = p.WholeSaler) SET l.FK_ProdID = p.ProdID  WHERE p.WholeSaler = " . $wholesalerID;
if (!mysqli_query($link, $updateLocationProdIDQuery)) {
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to update the FK_ProdID in the Locations table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    $error = 1;
} else {
    $count_location_prod = mysqli_affected_rows($link);
    $running_log .= "\nSuccessfully updated the FK_ProdID for " . $count_location_prod . " products in the Locations table.";    
}
*/
//Update FK_ProdID in MAP Table
$updateMAPProdIDQuery = "UPDATE " . $map_table_name . " m JOIN " . $products_table_name . " p on (m.VenderSKU = p.VenderSKU) SET m.FK_ProdID = p.ProdID WHERE p.WholeSaler = " . $wholesalerID;
if (!mysqli_query($link, $updateMAPProdIDQuery )) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the FK_ProdID in the MAP table.";
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    } else {
        $map_update_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the FK_ProdID for " . $map_update_count . " products in the MAP table.";       
}

//Update FK_ProdID in MSRP Table
/**/
$updateMSRPProdIDQuery = "UPDATE " . $msrp_table_name . " m JOIN " . $products_table_name . " p on (m.VenderSKU = p.VenderSKU) SET m.FK_ProdID = p.ProdID WHERE p.WholeSaler = " . $wholesalerID;
if (!mysqli_query($link, $updateMSRPProdIDQuery )) {
			//This is a non-fatal error.  Log and continue
			$running_log .= "\nFailed to update the FK_ProdID in the MSRP table.";
			$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
		} else {
			$map_update_count = mysqli_affected_rows($link);
			$running_log .= "\nSuccessfully updated the FK_ProdID for " . $map_update_count . " products in the MSRP table.";       
}


//Update FK_ProdID in Categories Table
/*
$updateCategoriesProdIDQuery = "UPDATE " . $categories_table_name . " m JOIN " . $products_table_name . " p on (m.VenderSKU = p.VenderSKU) SET m.FK_ProdID = p.ProdID WHERE p.WholeSaler = " . $wholesalerID;
if (!mysqli_query($link, $updateCategoriesProdIDQuery )) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the FK_ProdID in the categories table.";
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    } else {
        $category_update_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the FK_ProdID for " . $category_update_count . " products in the categories table.";       
}
*/

//Update FK_ProdID in images Table
$updateImagesProdIDQuery = "UPDATE " . $images_table_name . " m JOIN " . $products_table_name . " p on (m.FK_VenderSKU = p.VenderSKU) SET m.FK_ProdID = p.ProdID WHERE p.WholeSaler = " . $wholesalerID;
if (!mysqli_query($link, $updateImagesProdIDQuery )) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the FK_ProdID in the images table.";
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    } else {
        $images_update_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the FK_ProdID for " . $images_update_count . " products in the images table.";       
}

//Update FK_ProdID in notes_table_name Table
/*
$updateImagesProdIDQuery = "UPDATE " . $notes_table_name . " m JOIN " . $products_table_name . " p on (m.FK_VenderSKU = p.VenderSKU) SET m.FK_ProdID = p.ProdID WHERE p.WholeSaler = " . $wholesalerID;
if (!mysqli_query($link, $updateImagesProdIDQuery )) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to update the FK_ProdID in the notes_table_name table.";
        array_push($running_msg, "\nFailed to update the FK_ProdID in the notes_table_name table.");
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        array_push($running_msg, "The SQL error was " . mysqli_error($link) . ".");
    } else {
        $images_update_count = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully updated the FK_ProdID for " . $images_update_count . " products in the notes_table_name table.";       
        array_push($running_msg, "Successfully updated the FK_ProdID for " . $images_update_count . " products in the notes_table_name table.");
        
    }
*/

//Any products that are still locked were missing in the csv file, set them as discontinued
if ($error == 0) {
    $discontinueQuery = "UPDATE " . $products_table_name . " SET LOCK_DISC = 1, LOCK_UPDATE_QUAN = 0, ProdQuantity = 0, Active = 0 WHERE LOCK_UPDATE_QUAN = 1 AND WholeSaler = " . $wholesalerID;
    if (!mysqli_query($link, $discontinueQuery)) {
        //This is a non-fatal error.  Log and continue
        $running_log .= "\nFailed to set products to discontinued.";
        array_push($running_msg,"Failed to set products to discontinued.");
        $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        array_push($running_msg,"The SQL error was " . mysqli_error($link) . ".");
        $error = 1;
    } else {
        $discproducts = mysqli_affected_rows($link);
        $running_log .= "\nSuccessfully set " . $discproducts . " products as discontinued.";
        array_push($running_msg,"Successfully set " . $discproducts . " products as discontinued.");
    }
} else {
    $running_log .= "\nDidn't set any products to discontinued as a safeguard because errors were encountered.";
    array_push($running_msg,"Didn't set any products to discontinued as a safeguard because errors were encountered.");
    $discproducts = 0;
}

//Drop the temp table
$dropTempQuery = "DROP TABLE " . $temp_table_name;

if (!mysqli_query($link, $dropTempQuery)) { //Drop table failed.  Stop the update.
    //This is a non-fatal error.  Log and continue
    $running_log .= "\nFailed to drop the TEMP table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    $error = 1;
} else {
    $running_log .= "\nTEMP table was successfully dropped.";
}



$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;


$running_log .= "\n\n\nTotal Execution Time: ".$execution_time." Mins";
array_push($running_msg, "Total Execution Time: ".$execution_time." Mins");  
$running_log .= "\nProcessed " . $lines . " products \n";
array_push($running_msg,"Processed " . $lines . " products");
$running_log .= "\nThere were " . $update_count . " products updated and " . $newproducts . " products added.\n";
array_push($running_msg,"There were " . $update_count . " products updated and " . $newproducts . " products added.");
$running_log .= "\nThere were " . $discproducts . " products marked as discontinued.\n";
array_push($running_msg,"There were " . $discproducts . " products marked as discontinued.");

//Send Email on success
			$message = "The $SUPPLIER_NAME products FULL sync script finished successfully.  The script log is below:\n".$running_log;
			$subject ="The $SUPPLIER_NAME FULL Sync Script Completed Successfully";
			Henry_Email($subject, $message,$mailto,$cc,$bcc,false,true);
$feedObject = NULL;

$running_log .= "\nCleaning up downloads...";
array_push($running_msg, "Cleaning up downloads...");
unlink($local_file);
unlink($local_file2);
unlink($DownloadFile);

//DEBUG
print_r( $running_msg);

/*
foreach($running_msg as $i){
 xdebug_print_function_stack($i);
}
*/
mysqli_close($link);
?>
