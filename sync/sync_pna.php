<?php
 //Define location of required files in baltauto directory
require (dirname(__FILE__) . '/../includes/config.php');


//Change the PHP script timeout to infinite
set_time_limit(0);

//Increase the memory limit
ini_set('memory_limit', '-1');

$running_log = '';
$running_msg = array();
$newproducts = 0;
$updatedproducts = 0;
$lines = 0;
$query = array();
$progress = 0;
$error = 0;

$temp_table_name = $SUPPLIER_SHORT_NAME."_pna_temp";


$time_start = microtime(true);

$running_log .= "\nStarting...";
array_push($running_msg, "Starting...");
/**/
// Download inventory file from FTP
$local_file = dirname(__FILE__) ."/../downloads/".$SUPPLIER_SHORT_NAME."feed-PNA-".date('m-d-Y-h.i.s').".csv";
$DownloadFile = dirname(__FILE__) ."/../downloads/".$SUPPLIER_SHORT_NAME."feed-PNA-".date('m-d-Y-h.i.s').".xls";

//This function will kill the script if the feed file hasn't been updated since the last sync
if (copy( $ftp_filename_PNA,$DownloadFile)) {
    $running_log .= "\nSuccessfully downloaded update file to $DownloadFile";
    array_push($running_msg, "Successfully downloaded update file to $DownloadFile");
} else {
    $running_log .= "\nFailed to copy the file";
    array_push($running_msg, "Failed to copy the file");
    $message = "The script failed to download the update file.  The log below should show any errors that were captured:<br><br>".$running_log;
    mail($mailTo, "$SUPPLIER_NAME Sync Script Failed", $message);    
    die($message);
}

convertXLStoCSV($DownloadFile, $local_file,1);
unlink($DownloadFile);

fileUpdated_2($local_file,2);

//die($running_log);
$feedObject = createFileObject($local_file,",",1);




$running_log .= "\nCreating file object from download.";
array_push($running_msg, "Creating file object from download ");

while (!$feedObject->eof()) { 
    $lines++; 
    $feedObject->fgetcsv(); 
}

$lines--;

$running_log .= "\nThe file object contains " . $lines . " items.";
/**/
if ($lines > 1) {
    $running_log .= "\nThe file object was successfully created.  Uploading feed file to S3 for archiving";
    
    if (SendToS3($local_file, $bucket_name, $access_policy)) {
        $running_log .= "\nFile successfully copied to S3 {$bucket_name}/".baseName($local_file).PHP_EOL;
    } else {
        $running_log .= "\nFailed to copy the file to S3!";
    }
} else {
    $running_log .= "\nThere was a problem creating the file object!";
       syncFatalError(2, $running_log);

}


//Drop the temp table
$dropTempQuery = "DROP TABLE IF EXISTS " . $temp_table_name;
if (!mysqli_query($link, $dropTempQuery)) { //Drop table failed.  Stop the update.
    $running_log .= "\nFailed to drop the TEMP table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    
       syncFatalError(2, $running_log);

} 


//Create the temp table
$createTempTableQuery = "CREATE TABLE IF NOT EXISTS " . $temp_table_name . " (
                    `ProdID` int(11) NOT NULL auto_increment,
                    `ProdQuantity` int(11) NOT NULL default '0',
                    `WholeSalePrice` double(15,2) default NULL,					
                    `LOC1` int(11) NOT NULL default '0',
                    `VenderSKU` varchar(50) NOT NULL default '',
					`is_updated` int(1) default 0,
                    PRIMARY KEY  (`ProdID`)
                  );
";

if (!mysqli_query($link, $createTempTableQuery)) {
    $running_log .= "\nFailed to create the TEMP table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    
       syncFatalError(2, $running_log);

} else {
    $running_log .= "\nTEMP table was successfully created.";
}


foreach ($feedObject as $item) {
    
    $progress++;
	
	//DEBUG
	//print_r($item);
    
    //Check if the row is blank
    if ($item[0] != '') { 
 
        // Item Data
        $VenderSKU = TRIM(mysqli_real_escape_string($link, $item[0]));
        $LOC1 = TRIM(mysqli_real_escape_string($link, $item[2]));
        $WholeSalePrice = TRIM(mysqli_real_escape_string($link, $item[4]));
        //$LOC2 = mysqli_real_escape_string($link, $item[3]);
        $ProdQuantity =  $LOC1;
	
        $query[] = "INSERT INTO " . $temp_table_name . " 
		(ProdQuantity, 
		VenderSKU, 
		WholeSalePrice, 
		LOC1) 
		VALUES (
		'$ProdQuantity', 
		'$VenderSKU', 
		'$WholeSalePrice', 
		'$LOC1')";
	}
    
    if (count($query) == 1000) {
        
        //DEBUG
        //echo $query;
        
        $query_string = implode(';', $query);
        
        $i = 0;
        
        //After we have 1000 products, do a bulk insert
        if (mysqli_multi_query($link, $query_string)) {
            do {
                mysqli_next_result($link);
                $i++;
            }
            
            while( mysqli_more_results($link) );
        }
        
        if (mysqli_errno($link)) {
            $running_log .= "\nEncountered a SQL error!";
            $running_log .= "\nQuery number " . $i . " of 1000 failed";
            $running_log .= "\nFocus around line " . ($progress - (1000 - $i)) . " of " . $lines . " in the feed";
            $running_log .= "\nContext:";
            $running_log .= "\nQuery # " . ($i - 7) . " = " . $query[($i - 7)];
            $running_log .= "\nQuery # " . ($i - 6) . " = " . $query[($i - 6)];
            $running_log .= "\nQuery # " . ($i - 5) . " = " . $query[($i - 5)];
            $running_log .= "\nQuery # " . ($i - 4) . " = " . $query[($i - 4)];
            $running_log .= "\nQuery # " . ($i - 3) . " = " . $query[($i - 3)];
            $running_log .= "\nQuery # " . ($i - 2) . " = " . $query[($i - 2)];
            $running_log .= "\nQuery # " . ($i - 1) . " = " . $query[($i - 1)];
            $running_log .= "\nQuery # " . $i . " = " . $query[$i];
            $running_log .= "\nQuery # " . ($i + 1) . " = " . $query[($i + 1)];
            $running_log .= "\nQuery # " . ($i + 2) . " = " . $query[($i + 2)];
            $running_log .= "\nError = " . mysqli_error($link);
            echo $running_log;
            $message = "The script failed during a SQL query.  The log below should show any errors that were captured:<br><br>".$running_log;
            mail($mailTo, "Green Supply Script Failed", $message);    
            die();        
        }           

        unset($query);
    }
}

//Take care of any leftovers
$query_string = implode(';', $query);
$i = 0;
        
if (mysqli_multi_query($link, $query_string)) {
    do {
        mysqli_next_result($link);
        $i++;
    }
            
        while( mysqli_more_results($link) );
}
        
if (mysqli_errno($link)) {
    $running_log .= "\nEncountered a SQL error!";
    $running_log .= "\nQuery number " . $i . " of 1000 failed";
    $running_log .= "\nFocus around line " . ($progress - (1000 - $i)) . " of " . $lines . " in the feed";
    $running_log .= "\nContext:";
    $running_log .= "\nQuery # " . ($i - 7) . " = " . $query[($i - 7)];
    $running_log .= "\nQuery # " . ($i - 6) . " = " . $query[($i - 6)];
    $running_log .= "\nQuery # " . ($i - 5) . " = " . $query[($i - 5)];
    $running_log .= "\nQuery # " . ($i - 4) . " = " . $query[($i - 4)];
    $running_log .= "\nQuery # " . ($i - 3) . " = " . $query[($i - 3)];
    $running_log .= "\nQuery # " . ($i - 2) . " = " . $query[($i - 2)];
    $running_log .= "\nQuery # " . ($i - 1) . " = " . $query[($i - 1)];
    $running_log .= "\nQuery # " . $i . " = " . $query[$i];
    $running_log .= "\nQuery # " . ($i + 1) . " = " . $query[($i + 1)];
    $running_log .= "\nQuery # " . ($i + 2) . " = " . $query[($i + 2)];
    $running_log .= "\nError = " . mysqli_error($link);
    echo $running_log;
    $message = "The script failed during a SQL query.  The log below should show any errors that were captured:<br><br>".$running_log;
    mail($mailTo, "Green Supply Script Failed", $message);    
    die();        
}           

$running_log .= "\nFeed loaded into the TEMP table.";

/*set quantity to 0 for brands that dont updte right

					$updateLocationProdIDQuery = "update " . $temp_table_name . " a JOIN products b on a.`VenderSKU` = b.`VenderSKU` set  a.`ProdQuantity` = 0 WHERE b.Manufacturer = 'TRUCK LITE' AND WholeSaler = " . $wholesalerID;
					if (!mysqli_query($link, $updateLocationProdIDQuery)) {
						//This is a non-fatal error.  Log and continue
						$running_log .= "\nFailed to update the restricted quan.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						$error = 1;
					} else {
						$count_location_prod = mysqli_affected_rows($link);
						$running_log .= "\nSuccessfully updated the restricted quan. " . $count_location_prod . " products in the  table.";    
					}
*/
//TESTING ; when testing this is good to uncomment to kill the script right here after all data is imported into the temp table
//die($running_log);
				//Set the flag for updates
				$startQuery = "UPDATE " . $products_table_name . " SET LOCK_UPDATE_QUAN = 1 WHERE WholeSaler = " . $wholesalerID;

				if (!mysqli_query($link, $startQuery)) {
					//This is a non-fatal error.  Log and continue
					$running_log .= "\nFailed to set the update flag.";
					$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
					$error = 1;
					
					if (mysqli_errno($link) == "1205") {
						$running_log .= "\nThe products table is currently locked.  Will try again later";
						   syncFatalError(2, $running_log);
        
					}
					
					   syncFatalError(2, $running_log);
  
				} else {
					$flag_count = mysqli_affected_rows($link);
					$running_log .= "\nSuccessfully set the update flag on for " . $flag_count . " products.";
				}
//Update Item locations table from the temp table
/*                	*/
$countLocationQuery = "SELECT * FROM " . $location_table_name . " WHERE FK_WholesalerID = " . $wholesalerID;
if (!mysqli_query($link, $countLocationQuery)){
    $running_log .= "\nFailed to select the number of products in the locations table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        
       syncFatalError(2, $running_log);

} else {
    $location_count = mysqli_affected_rows($link);
}
    
					$deleteLocationsQuery = "DELETE FROM " . $location_table_name . " WHERE FK_WholesalerID = " . $wholesalerID;
					if (!mysqli_query($link, $deleteLocationsQuery)) {
						$running_log .= "\nFailed to delete from the product locations table.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
							
						   syncFatalError(2, $running_log);

					} else {
						$delete_count = mysqli_affected_rows($link);
							
						if ($location_count != $delete_count) {
							$running_log .= "\nThe number of rows deleted from the locations table does not equal what should have been deleted.  Something happened.";
							$running_log .= "\nThere were " . $location_count . " items in the location table but only " . $delete_count . " items were deleted.";
							   syncFatalError(2, $running_log);

						} else {
							$running_log .= "\nSuccessfully deleted the old product locations.";
						}
							
					}
						
					$updateLocationsQuery = "INSERT INTO " . $location_table_name . " (VenderSKU,FK_WholesalerID,LOC1) (SELECT VenderSKU, " . $wholesalerID . ", LOC1 FROM " . $temp_table_name . ")";
					if (!mysqli_query($link, $updateLocationsQuery)){
						$running_log .= "\nFailed to update the locations table.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						  
						   syncFatalError(2, $running_log);

					} else {
						$running_log .= "\nSuccessfully updated the current product locations.";
					}
				
					//Update the prices from the temp table
					$updatePriceQuery = "UPDATE " . $products_table_name . " p, " . $temp_table_name . " t SET p.WholeSalePrice = t.WholeSalePrice, p.LOCK_DISC = 0, p.LOCK_UPDATE_QUAN = 0, p.Active = 1 WHERE p.VenderSKU = t.VenderSKU  AND p.WholeSaler = " . $wholesalerID . " AND p.LOCK_UPDATE_PRICE = 0";
					if (!mysqli_query($link, $updatePriceQuery)){
						$running_log .= "\nFailed to update the product prices.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						
						if (mysqli_errno($link) == "1205") {
							$running_log .= "\nThe products table is currently locked.  Will try again later";
							   syncFatalError(2, $running_log);
        
						}
						   syncFatalError(2, $running_log);

					} else {
						$running_log .= "\nSuccessfully updated the product prices.";
						$update_count = mysqli_affected_rows($link);
					}
					
					//Update FK_ProdID in Locations Table
					/*	*/$updateLocationProdIDQuery = "UPDATE " . $location_table_name . " l JOIN " . $products_table_name . " p on (l.VenderSKU = p.VenderSKU AND l.FK_WholesalerID = p.WholeSaler) SET l.FK_ProdID = p.ProdID  WHERE p.WholeSaler = " . $wholesalerID;
					if (!mysqli_query($link, $updateLocationProdIDQuery)) {
						//This is a non-fatal error.  Log and continue
						$running_log .= "\nFailed to update the FK_ProdID in the Locations table.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						$error = 1;
					} else {
						$count_location_prod = mysqli_affected_rows($link);
						$running_log .= "\nSuccessfully updated the FK_ProdID for " . $count_location_prod . " products in the Locations table.";    
					}
				$running_log .= "\n\nUpdating product quantity";  
			
                               
                                
				//Update the product details from temp table
				
				$updateDetailQuery = "UPDATE " . $products_table_name . " p, " . $temp_table_name . " t SET p.ProdQuantity = t.ProdQuantity, p.TimeStamp = NOW(), LOCK_UPDATE_QUAN = 0 WHERE p.VenderSKU = t.VenderSKU  AND p.WholeSaler = ".$wholesalerID;
				if (!mysqli_query($link, $updateDetailQuery)) {
					$running_log .= "\nFailed to update the product details and quantities.";
					$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						
					if (mysqli_errno($link) == "1205") {
						$running_log .= "\nThe products table is currently locked.  Will try again later";
						   syncFatalError(2, $running_log);
        
					}
						
					   syncFatalError(2, $running_log);

				} else {
					$running_log .= "\nSuccessfully updated the product details and quantities.";
					$update_count = mysqli_affected_rows($link);        
				}
				//Any products that are still locked were missing in the csv file, set them as discontinued
				if ($error == 0) {
					$discontinueQuery = "UPDATE " . $products_table_name . " SET LOCK_DISC = 1, LOCK_UPDATE_QUAN = 0, ProdQuantity = 0, Active = 0 WHERE LOCK_UPDATE_QUAN = 1 AND WholeSaler = " . $wholesalerID;
					if (!mysqli_query($link, $discontinueQuery)) {
						//This is a non-fatal error.  Log and continue
						$running_log .= "\nFailed to set products to discontinued.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						$error = 1;
					} else {
						$discproducts = mysqli_affected_rows($link);
						$running_log .= "\nSuccessfully set " . $discproducts . " products as discontinued.";
					}
				} else {
					$running_log .= "\nDidn't set any products to discontinued as a safeguard because errors were encountered.";
					$discproducts = 0;
				}		

				
$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;

$running_log .= "\n\n\nTotal Execution Time: ".$execution_time." Mins";
$running_log .= "\nProcessed " . $lines . " products \n";
$running_log .= "\nThere were " . $update_count . " products updated and " . $newproducts . " products added.\n";
$running_log .= "\nThere were " . $discproducts . " products marked as discontinued.\n";


//Send Email on success
			$message = "$SUPPLIER_NAME products PNA sync script finished successfully.  The script log is below:\n".$running_log;
			$subject ="$SUPPLIER_NAME PNA Sync Script Completed Successfully";
			Henry_Email($subject, $message,$mailto,$cc,$bcc,false,true);
$feedObject = NULL;

$running_log .= "\nCleaning up downloads...";
unlink($local_file);

//DEBUG
echo( $running_log);

mysqli_close($link);
?>
