<?php

function createFileObject($filename='',$del,$skip =1) {
    if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

        $file = new SplFileObject($filename);
        $file->setFlags(SplFileObject::READ_CSV);
        
		//Use this to set the delimiters and enclosures if needed
		$file->setCsvControl($del);
		
		//Skip the 1st row (header) if needed
		$file = new LimitIterator($file, $skip);

        return $file;
}
function fileUpdated2($file,$type){
    $datefile = dirname(__FILE__) . '/date_'.$type.'.txt';
    
    $last_hash = file_get_contents($datefile);

    $current_hash = hash_file('md5', $file);
    
	echo "does $current_hash = $last_hash";
	
    if ($last_hash != $current_hash) { //The file is new
		
		file_put_contents($datefile, $current_hash);
        return true;
        
    } else { //The file hasn't changed
        file_put_contents($datefile, $current_hash);
         //delete downloaded
		unlink($file);       
        // Do not run full sync
        die("File not updated");
    }
}
function fileUpdated($ftp, $file,$type){
    $datefile = dirname(__FILE__) . '/date_'.$type.'.txt';
    
    $lastTimeStamp = file_get_contents($datefile);
    $currentTimeStamp = ftp_mdtm($ftp, $file);
    
    if ($lastTimeStamp != $currentTimeStamp) { //The file is new
        
        $original_file_size = ftp_size($ftp, $file);
        sleep(10);
        $current_file_size = ftp_size($ftp, $file);
        
        if ($original_file_size == $current_file_size) { //File is not uploading
            file_put_contents($datefile, $currentTimeStamp);
            return true;
        } else { //File is still uploading.... pause 30 seconds and try again
            sleep(30);
            
            $original_file_size = ftp_size($ftp, $file);
            sleep(10);
            $current_file_size = ftp_size($ftp, $file);
        
            if ($current_file_size == $original_file_size) {
                file_put_contents($datefile, $currentTimeStamp);
                return true;
            } else { //Upload is ongoing stop script and try later
                die();
            }
        }
    } else { //The file hasn't changed
        file_put_contents($datefile, $currentTimeStamp);
            //delete downloaded
		//unlink($file);           
        // Do not run full sync
        die("File not updated");
    }
}
function fileUpdated_2($file,$type) {
    $datefile = dirname(__FILE__) . '/date_'.$type.'.txt';
    
    $last_hash = file_get_contents($datefile);

    $current_hash = hash_file('md5', $file);
    
	echo "does $current_hash = $last_hash";
	
    if ($last_hash != $current_hash) { //The file is new
		
		file_put_contents($datefile, $current_hash);
        return true;
        
    } else { //The file hasn't changed
        file_put_contents($datefile, $current_hash);
         //delete downloaded
		unlink($file);       
        // Do not run full sync
        die("File not updated");
    }
}

function syncFatalError($type, $running_log){ //Type can be 1 for stock or 2 for dropship
	GLOBAL $mailto;
	GLOBAL $cc;
	GLOBAL $bcc;
	GLOBAL $SUPPLIER_NAME;
    $datefile = array();
    $subject = array();
    $message = array();
    
    $datefile = array();
    $subject = array();
    $message = array();
    
    $datefile = dirname(__FILE__) . '/date_'.$type.'.txt';


    
    $subject[1] = "$SUPPLIER_NAME FULL Sync Script Failed";
    $subject[2] = "$SUPPLIER_NAME PNA  Sync Script Failed";
    
    $message[1] = "The $SUPPLIER_NAME FULL sync script failed.  The script log is below:\n" . $running_log;
    $message[2] = "The $SUPPLIER_NAME PNA sync script failed.  The script log is below:\n" . $running_log;
	
    $subject = $subject[$type];
    $message = $message[$type]." The script log is below:\n" . $running_log;
        
    //Change the timestamp to 0 to guarantee an update next time.
    file_put_contents($datefile, '0');
    
   // mail($mailTo, $subject, $message);    
   
		Henry_Email($subject, $message,$mailto,$cc,$bcc,false,true);
		trigger_error($message, E_USER_ERROR);
	die();
}
function downloadURL($url, $file) {
    ### INITIALIZE CURL
    $ch = curl_init();

	### SET OUR OPTIONS
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
	
	### NOW LETS GRAB THE PAGE WE WANT
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_POST, false);
	$result = curl_exec($ch);

	if (curl_error($ch)) {
        echo "<p>" . curl_error($ch)."</p>";
    }
    
	$feed = fopen($file, "w+");
	fputs($feed, $result);
	fclose($feed);
	
	curl_close($ch);
    
	return $result;
	
}

function clean($string) {
	$temp = preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
   return str_replace("  "," ",$temp); //return with double spaces repved
}


function getSessionID($url) {
	### INITIALIZE CURL
	$ch = curl_init();

	## SET OUR OPTIONS
	curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__).'/cookie.txt');
	curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__).'/cookie.txt');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_POST, false);
	
	
	$result = curl_exec($ch);
	
	if (curl_error($ch)) {
        echo "<p>" . curl_error($ch)."</p>";
		return false;
    }
	
	$loggedIn = strpos($result, 'Nathan Franco');
	
	if ($loggedIn) { //There will be no session id on this page if the previous cookie is still valid so check to see if logged in.
		return '0';
	}
	
	curl_close($ch);
	
	
	if (preg_match("/Login_Form_SessionID' value='(.*)'>/", $result, $sessionId)) {
		return  $sessionId[1];
	} else {
		return false;
	}		
}

function webLogin($url, $username, $password, $session_id) {
	### INITIALIZE CURL
	$ch = curl_init();

	## SET OUR OPTIONS
	curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__).'/cookie.txt');
	curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__).'/cookie.txt');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_POST, true);
	
	$post_fields = array(
		'Login_Form_txtUser' => $username,
		'Login_Form_txtPass' => $password,
		'Login_Form_SessionID' => $session_id,
		'Login_Form_SendTo' => 'Users/ManageAccounts.aspx',
		'Login_Form_Mode' => 'Login',
		'Form' => 'Login_Form'
	);
	
	$post_string = http_build_query($post_fields);
	
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	
	$result = curl_exec($ch);
	
	if (curl_error($ch)) {
        echo "<p>" . curl_error($ch)."</p>";
		return false;
    }
	
	curl_close($ch);
	
	$loggedIn = strpos($result, 'Nathan Franco');
	
	if ($loggedIn) {
		return true;
	} else {
		return false;
	}
}

function downloadReport($url, $session_id, $file) {
	### INITIALIZE CURL
	$ch = curl_init();

	## SET OUR OPTIONS
	curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__).'/cookie.txt');
	curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__).'/cookie.txt');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_POST, true);
	
	$post_fields = array(
		'Report1010002_Form_Format' => 'CSV',
		'Report1010002_Form_SessionID' => $session_id,
		'Form' => 'Report1010002_Form',
		'Report1010002_Form_Mode' => 'Run',
		'Report1010002_Form_Sort' => ''
	);
	
	$post_string = http_build_query($post_fields);
	
	
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	
	$result = curl_exec($ch);
	
	if (curl_error($ch)) {
        echo "<p>" . curl_error($ch)."</p>";
		return false;
    }
	
	curl_close($ch);
	
	$feed = fopen($file, "w+");
	fputs($feed, $result);
	fclose($feed);

	
	return $result;
}
function convertXLStoCSV($infile,$outfile) {
	
	require_once(dirname(__FILE__ ) . '/../../lgcustomincludes/PHPExcel/Classes/PHPExcel.php');
	
    $fileType = PHPExcel_IOFactory::identify($infile);
    $objReader = PHPExcel_IOFactory::createReader($fileType);
 
    $objReader->setReadDataOnly(true);   
    $objPHPExcel = $objReader->load($infile);    
 
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save($outfile);
}
function convertXLStoCSV_Multi_Sheet($infile,$outfile,$index) {
	
	require_once(dirname(__FILE__ ) . '/../../lgcustomincludes/PHPExcel/Classes/PHPExcel.php');
	
    $fileType = PHPExcel_IOFactory::identify($infile);
    $objReader = PHPExcel_IOFactory::createReader($fileType);
 
    $objReader->setReadDataOnly(true);   
    $objPHPExcel = $objReader->load($infile);    
 	$objPHPExcel->setActiveSheetIndex($index);
		//	$index = 0;

			$objPHPExcel->setActiveSheetIndex($index);

			// write out each worksheet to it's name with CSV extension
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			$objWriter->setSheetIndex($index);
			$objWriter->save($outfile);

   //
   // $objWriter->save($outfile);



}
?>
